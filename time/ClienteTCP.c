#include "ClienteTCP.h"

int sockfd;

void sigint_handlerTCP(int signo){
	puts("SIGINT received...");
	exit(0);
}
void TCPClient(int port,char* host,int dep, int x){
	uint32_t xu=htonl(x);

	//printf("%" PRIu32 "\n",xu);
  struct hostent *server;
	struct sockaddr_in serveraddr;
  int n;
  uint32_t seconds;

  /* handler SIGINT signal*/
  signal(SIGINT, sigint_handlerTCP);

  /* socket: create the socket */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
          perror("ERROR opening socket");
          exit(0);
  }
	if(dep==1){printf("Socket creado con identificador: %d\n", sockfd);}
  /* gethostbyname: get the server's DNS entry */
  server = gethostbyname(host);
  if (server == NULL) {
          fprintf(stderr,"ERROR, no such host: %s\n", host);
          exit(0);
  }

  /* build the server's Internet address */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
  serveraddr.sin_port = htons(port);
	if(dep==1){printf("Sistema intentará conectar con el servidor %s por el puerto %d\n", host, port);}

  /* connect: create a connection with the server */
  if (connect(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
          perror("ERROR connecting");
          exit(0);
  }
	if(dep==1){printf("Conectado a %s por el puerto %d\n", host, port);}

	n=send(sockfd,&xu,sizeof(uint32_t),0);
	if (n < 0) {
					perror("ERROR writing to socket");
					exit(0);
	}
	if(dep==1){printf("%d mandado a %s\n",x,host);}

	for(int i=0;i<5;i++){
    n = recv(sockfd, &seconds,sizeof(uint32_t), 0);

    if (n < 0) {
      perror("ERROR reading from socket");
      exit(0);
      }
      uint32_t secondsh=ntohl(seconds);

      char my_time[50];
  		struct tm *tmp;
  		time_t t= secondsh-2208988800;
  		tmp=localtime(&t);
  		strftime(my_time,sizeof(my_time),"%a %b %d %H:%M:%S %Z %Y",tmp);
  		if(n>0) printf("%s\n",my_time);

		}
		close(sockfd);
		if(dep==1){printf("Socket cerrado\n");}
}
