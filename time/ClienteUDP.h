#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>

void sigint_handlerUDP(int signo);
void UDPClient (int port, char* host,int dep);
